import Head from 'next/head'
import SideBar from '../components/SideBar'
import Center from '../components/Center'
import { getSession, signIn, useSession } from 'next-auth/react'
import Player from '../components/Player'
import { GetServerSidePropsContext } from 'next'

export default function Home() {
  useSession({
    required: true,
    onUnauthenticated() {
      signIn()
    }
  })

  return (
    <div className="h-screen bg-black overflow-hidden">
      <Head>
        <title>Spotify 2.0</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex w-full flex-1">
        <SideBar />
        <Center />
      </main>
      <div className="sticky bottom-0">
        <Player />
      </div>
    </div>
  )
}

export async function getServerSideProps(context: GetServerSidePropsContext){
  const session = await getSession(context)

  return {
    props: {
      session
    }
  }
}