import { DefaultUser, Session, User } from "next-auth";
import { JWT } from "next-auth/jwt";

export interface INextAuthJWT extends JWT {
    accessToken: string
    refreshToken: string
    username: string
    accessTokenExpires: number
}

export interface INextAuthSession extends Session {
    user: INextAuthUser
    error?: INextAuthApiError
}

export interface INextAuthUser extends DefaultUser {
    accessToken: string
    refreshToken: string
    username: string
    accessTokenExpires: number
}
export interface INextAuthSessionContext {
    session: INextAuthSession
    user: User
    token: INextAuthJWT
}

export interface INextAuthApiError extends JWT {
    error: string
}