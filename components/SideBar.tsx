import React, { useContext, useEffect, useState } from 'react'
import { HomeIcon, LibraryIcon, LogoutIcon, PlusCircleIcon, SearchIcon } from '@heroicons/react/outline'
import { HeartIcon } from '@heroicons/react/solid'
import { signOut, useSession } from 'next-auth/react'
import useSpotify from '../hooks/useSpotify'
import { ISpotifyPlaylist } from '../interfaces/spotify.interface'
import { SpotifyPlaylistContext, SpotifyPlaylistIdContext } from '../contexts/spotify.context'

const SideBar = () => {
    const spotifyApi = useSpotify()
    const { data: session } = useSession()
    const [playlists, setPlaylists] = useState([])

    const { setPlaylist } = useContext(SpotifyPlaylistContext)
    const { setPlaylistId } = useContext(SpotifyPlaylistIdContext)

    useEffect(() => {
        if(spotifyApi.getAccessToken()){
            spotifyApi.getUserPlaylists()
                .then((data) => {
                    setPlaylists(data.body.items)
                    setPlaylistId(data.body.items[0].id)
                })
                .catch((error) => console.log(error))
        }
    }, [session, spotifyApi])

    return (
        <div className="hidden md:inline-flex flex-col text-gray-500 p-5 text-xs lg:text-sm border-r border-gray-900 sm:max-w-[12rem] lg:maw-w-[15rem] overflow-y-scroll h-screen pb-36">
          <div className="sticky">
              {session && (
                  <div className="mb-5">
                        <button className="flex items-center space-x-2 hover:text-white" onClick={() => signOut()}>
                            <LogoutIcon className="w-5 h-5" />
                            <p>Déconnexion</p>
                        </button>
                    </div>
              )}
            <div className="space-y-3 mb-5">
                <button className="flex items-center space-x-2 hover:text-white">
                    <HomeIcon className="w-5 h-5" />
                    <p>Accueil</p>
                </button>
                <button className="flex items-center space-x-2 hover:text-white">
                    <SearchIcon className="w-5 h-5" />
                    <p>Rechercher</p>
                </button>
                <button className="flex items-center space-x-2 hover:text-white">
                    <LibraryIcon className="w-5 h-5" />
                    <p>Bibliothèque</p>
                </button>
            </div>
            <div className="space-y-3">
                <button className="flex items-center space-x-2 hover:text-white">
                    <PlusCircleIcon className="w-5 h-5" />
                    <p>Créer une playlist</p>
                </button>
                <button className="flex items-center space-x-2 hover:text-white">
                    <HeartIcon className="text-blue-500 w-5 h-5" />
                    <p>Titres likés</p>
                </button>
            </div>
            <hr className="border-t my-5" />
          </div>

          {/* Playlists */}
          <div className="overflow-y-auto">
            {playlists.map((playlist: ISpotifyPlaylist) => (
                <p key={playlist.id} onClick={() => {
                    setPlaylistId(playlist.id)
                    setPlaylist(playlist)
                }} className="hover:text-white cursor-pointer">{playlist.name}</p>
            ))}
          </div>
        </div>
    )
}

export default SideBar
