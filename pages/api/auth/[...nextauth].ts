import NextAuth from 'next-auth'
import SpotifyProvider from 'next-auth/providers/spotify'
import { INextAuthApiError, INextAuthJWT, INextAuthSessionContext } from '../../../interfaces/next-auth.interface'
import spotifyApi, { LOGIN_URL, TOKEN_STATUS } from '../../../utils/spotify'

const refreshAccessToken = async (token: INextAuthJWT): Promise<INextAuthJWT | INextAuthApiError> => {
    try {
        spotifyApi.setAccessToken(token.accessToken)
        spotifyApi.setRefreshToken(token.refreshToken)

        const { body: refreshToken } = await spotifyApi.refreshAccessToken()

        return {
            ...token,
            accessToken: refreshToken.access_token,
            accessTokenExpires: Date.now() + refreshToken.expires_in * 1000,
            refreshToken: refreshToken.refresh_token ?? token.refreshToken
        }

        
    } catch (error) {
        console.error(error);
        return {
            ...token,
            error: TOKEN_STATUS.REFRESH_TOKEN_ERROR
        }
    }
}

export default NextAuth({
    providers: [
        SpotifyProvider({
            clientId: process.env.NEXT_PUBLIC_SPOTIFY_CLIENT_ID,
            clientSecret: process.env.NEXT_PUBLIC_SPOTIFY_CLIENT_SECRET,
            authorization: LOGIN_URL
        })
    ],
    secret: process.env.JWT_SECRET,
    pages: {
        signIn: '/login'
    },
    callbacks: {
        async jwt({ token, account, user }) {
            // Initial SignIn
            if(account && user) {
                return {
                    ...token,
                    accessToken: account.access_token,
                    refreshToken: account.refresh_token,
                    username: account.providerAccountId,
                    accessTokenExpires: account.expires_at * 1000
                }
            }

            if(Date.now() < token.accessTokenExpires) {
                return token
            }

            // Refresh the token
            return await refreshAccessToken(token as INextAuthJWT)
        },
        async session({ session, token }: INextAuthSessionContext){
            session.user.accessToken = token.accessToken
            session.user.refreshToken = token.refreshToken
            session.user.username = token.username

            return session
        }
    }
})