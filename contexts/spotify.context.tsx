import React from "react";

export const SpotifyPlaylistIdContext = React.createContext(null)
export const SpotifyPlaylistContext = React.createContext(null)
export const SpotifyCurrentTrackId = React.createContext(null)
export const SpotifyIsPlaying = React.createContext(null)
