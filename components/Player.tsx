import { PlayIcon, RewindIcon, SwitchHorizontalIcon, PauseIcon, FastForwardIcon, RefreshIcon, VolumeUpIcon } from '@heroicons/react/solid'
import { HeartIcon, VolumeUpIcon as VolumeDownIcon, RefreshIcon as RefreshOutlinedIcon } from '@heroicons/react/outline'
import { useSession } from 'next-auth/react'
import React, { useCallback, useContext, useEffect, useState } from 'react'
import debounce from  'lodash/debounce'

import { SpotifyCurrentTrackId, SpotifyIsPlaying } from '../contexts/spotify.context'
import useSongInfo from '../hooks/useSongInfo'
import useSpotify from '../hooks/useSpotify'
import { ISpotifySongInfos } from '../interfaces/spotify.interface'

const Player = () => {
    const spotifyApi = useSpotify()
    const { data: session } = useSession()
    const { currentTrackId, setCurrentTrackId } = useContext(SpotifyCurrentTrackId)
    const { isPlaying, setIsPlaying } = useContext(SpotifyIsPlaying)
    const [volume, setVolume] = useState(50)
    const [isRepeating, setIsRepeating] = useState(false)
    const songInfo: ISpotifySongInfos = useSongInfo()

    const fetchCurrentSong = async () => {
        if(!songInfo) {
            const data = await spotifyApi.getMyCurrentPlayingTrack()
            const currentPlayingTrackId = data.body?.item.id
            setCurrentTrackId(currentPlayingTrackId)

            const isPlayingState = await spotifyApi.getMyCurrentPlaybackState()
            setIsPlaying(isPlayingState.body?.is_playing)
        }
    }

    const handlePlayPause = async () => {
        const data = await spotifyApi.getMyCurrentPlaybackState()
        if(data.body.is_playing) {
            spotifyApi.pause()
            setIsPlaying(false)
        } else {
            spotifyApi.play()
            setIsPlaying(true)
        }
    }

    const handleRepeat = async () => {
        if(!isRepeating) {
            spotifyApi.setRepeat('track')
            setIsRepeating(true)
        } else {
            spotifyApi.setRepeat('off')
            setIsRepeating(false)
        }
    }
    useEffect(() => {
        if(spotifyApi.getAccessToken() && !currentTrackId) {
            fetchCurrentSong()
            setVolume(50)
        }
    }, [currentTrackId, spotifyApi, session])

    useEffect(() => {
        if(volume > 0 && volume < 100) {
            debouncedAdjustVolume(volume)
        }
    }, [volume])

    const debouncedAdjustVolume = useCallback(
        debounce((volume) => {
            spotifyApi.setVolume(volume)
                .catch((error) => {})
        }, 500), []
    )
    return (
        <section className="text-white h-24 bg-gradient-to-b from-black to-gray-900 grid grid-cols-3 text-xs md:text-base px-2 md:px-8">
            <article className="flex items-center space-x-4">
                <img
                    className="hidden md:inline w-10 h-10"
                    src={songInfo?.album.images[0]?.url}
                    alt={songInfo?.album.name}
                />
                <div>
                    <h3>{songInfo?.name}</h3>
                    <p>{songInfo?.artists?.[0]?.name}</p>
                </div>
            </article>

            <article className="flex items-center justify-evenly">
                <SwitchHorizontalIcon className="button" />
                <RewindIcon className="button" onClick={() => spotifyApi.skipToPrevious()} />
                { isPlaying ? (
                    <PauseIcon onClick={handlePlayPause} className="button w-10 h-10" />
                ) : (
                    <PlayIcon onClick={handlePlayPause} className="button w-10 h-10" />
                )}
                <FastForwardIcon className="button" onClick={() => spotifyApi.skipToNext()}/>
                {isRepeating ? (
                    <RefreshIcon className="button" onClick={handleRepeat}/>
                ) : (
                    <RefreshOutlinedIcon className="button" onClick={handleRepeat}/>
                )}
            </article>

            <article className="flex items-center space-x-3 md:space-x-4 justify-end pr-5">
                <VolumeDownIcon onClick={() => volume > 0 && setVolume(volume - 10)} className="button" />
                <input
                    className="w-14 md:w-28"
                    type="range"
                    value={volume}
                    min={0}
                    max={100}
                    onChange={(e) => setVolume(Number(e.target.value))}
                />
                <VolumeUpIcon onClick={() => volume < 100 && setVolume(volume + 10)} className="button" />
            </article>
        </section>
    )
}

export default Player
