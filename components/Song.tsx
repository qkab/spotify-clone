import React, { Dispatch, SetStateAction, useContext } from 'react'
import dayjs from 'dayjs'
import useSpotify from '../hooks/useSpotify'
import { ISpotifySong } from '../interfaces/spotify.interface'
import { SpotifyCurrentTrackId, SpotifyIsPlaying } from '../contexts/spotify.context'

interface ISpotifyCurrentTrackId {
    currentTrackId: string
    setCurrentTrackId: Dispatch<SetStateAction<any>>
}

interface ISpotifyIsPLaying {
    isPlaying: boolean
    setIsPlaying: Dispatch<SetStateAction<boolean>>
}

const Song: React.FC<{ track: ISpotifySong, order: number }> = ({ track, order }) => {
    const spotifyApi = useSpotify()
    const { currentTrackId, setCurrentTrackId }: ISpotifyCurrentTrackId = useContext(SpotifyCurrentTrackId)
    const { isPlaying, setIsPlaying }: ISpotifyIsPLaying = useContext(SpotifyIsPlaying)

    const playSong = async () => {
        try {
            setCurrentTrackId(track.track.id)
            setIsPlaying(true)
            await spotifyApi.play({
                uris: [track.track.uri]
            })
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <div
            className="grid grid-cols-2 text-gray-500 py-4 px-5 hover:bg-gray-900 rounded-lg cursor-pointer"
            onClick={playSong}
        >
            <div className="flex items-center space-x-4">
                <p>{order + 1}</p>
                <img className="h-10 w-10" src={track.track.album.images[0].url} alt={track.track.name} />
                <div>
                    <p className="w-36 lg:w-64 text-white truncate">{track.track.name}</p>
                    <p className="w-40">{track.track.artists[0].name}</p>
                </div>

            </div>

            <div className="flex items-center justify-between ml-auto md:ml-0">
                <p className="hidden md:inline w-40">{track.track.album.name}</p>
                <p>{dayjs(track.track.duration_ms).format('mm:ss')}</p>
            </div>
        </div>
    )
}

export default Song
