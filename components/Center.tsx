import { ChevronDownIcon } from '@heroicons/react/outline'
import { useSession } from 'next-auth/react'
import React, { useContext, useEffect, useState } from 'react'
import { shuffle } from 'lodash'
import { SpotifyPlaylistContext, SpotifyPlaylistIdContext } from '../contexts/spotify.context'
import { COLORS } from '../utils/spotify'
import useSpotify from '../hooks/useSpotify'
import Songs from './Songs'

const Center = () => {
  const { data: session } = useSession()
  const [color, setColor] = useState(null)
  const { playlistId } = useContext(SpotifyPlaylistIdContext)
  const { playlist, setPlaylist } = useContext(SpotifyPlaylistContext)
  const spotifyApi = useSpotify()

  useEffect(() => {
      setColor(shuffle(COLORS).pop())
  }, [playlistId])

  useEffect(()=>{
      if(playlistId !== '') {
        spotifyApi.getPlaylist(playlistId)
            .then((data) => setPlaylist(data.body))
            .catch((error) => console.log(error))
      }
  }, [spotifyApi, playlistId])

  return (
        <section className="flex-col flex-grow relative h-screen overflow-y-scroll">
            <header className="absolute top-5 right-8">
                <div className="flex items-center bg-red-300 opacity-90 hover:opacity-80 rounded-full font-bold p-1 pr-2 space-x-2">
                    <img className="w-10 h-10 rounded-full" src={session?.user.image} alt={session?.user.name} />
                    <h2>{session?.user.name}</h2>
                    <ChevronDownIcon className="w-5 h-5" />
                </div>
            </header>
          <article className={`flex items-end space-x-7 bg-gradient-to-b to-black ${color} text-white p-8 h-80`}>
            {/* Songs */}
            <img className="h-44 w-44 shadow-2xl" src={playlist?.images?.[0].url} alt={playlist?.name} />
            <div>
                <p>PLAYLIST</p>
                <h2 className="text-2xl md:text-3xl xl:text-5xl font-bold">{playlist?.name}</h2>
            </div>
          </article>
          <article>
            <div>
                <Songs />
            </div>
          </article>
        </section>
    )
}

export default Center
