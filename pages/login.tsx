import { LoginIcon } from '@heroicons/react/outline'
import { BuiltInProviderType } from 'next-auth/providers'
import { ClientSafeProvider, getProviders, LiteralUnion, signIn } from 'next-auth/react'
import React from 'react'

interface ILoginProps {
    providers: Record<LiteralUnion<BuiltInProviderType, string>, ClientSafeProvider>
}
const login: React.FC<ILoginProps> = ({ providers }) => {
    return (
        <div className="flex items-center justify-center flex-col h-screen bg-black">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-10 w-10 mb-5 bg-green-500 rounded-full p-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 19V6l12-3v13M9 19c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zm12-3c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zM9 10l12-3" />
            </svg>

            {Object.values(providers).map((provider) => (
                <button key={provider.id} className="flex items-center space-x-2 bg-green-500 rounded-lg p-3" onClick={() => signIn(provider.id, { callbackUrl: '/' })}>
                    <h4>Se connecter avec {provider.name}</h4>
                    <LoginIcon className="w-5 h-5" />
                </button>
            ))}
        </div>
    )
}

export default login

export async function getServerSideProps() {
    const providers = await getProviders()

    return {
        props: {
            providers
        }
    }
}
