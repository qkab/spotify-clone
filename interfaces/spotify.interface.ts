export interface ISpotifyExternalUrl {
    spotify: string
}

export interface ISpotifyImage {
    height: number
    url: string
    width: number
}

export interface ISpotifyOwner {
    display_name: string
    external_urls: ISpotifyExternalUrl
    href: string
    id: string
    type: string
    uri: string
}

export interface ISpotifyVideoThumbnail {
    url: string
}

export interface ISpotifyAlbum {
    album_type: string
    artists: ISpotifyArtist[]
    available_markets: ISpotifyAvailableMarket[]
    external_urls: ISpotifyExternalUrl
    href: string
    id: string
    images: ISpotifyImage[]
    name: string
    release_date: string
    release_date_precision: string
    total_tracks: number
    type: string
    uri: string
}

export interface ISpotifyArtist {
    name: string
    external_urls: ISpotifyExternalUrl
    href: string
    id: string
    type: string
    uri: string
}

export interface ISpotifyAvailableMarket {}
export interface ISpotifyExternalId {
    isrc: string
}

export interface ISpotifySongInfos {
    album: ISpotifyAlbum
    artists: ISpotifyArtist[]
    available_markets: ISpotifyAvailableMarket[]
    disc_number: number
    duration_ms: number
    episode: boolean
    explicit: boolean
    external_ids: ISpotifyExternalId[]
    external_urls: ISpotifyExternalUrl
    href: string
    id: string
    is_local: boolean
    name: string
    popularity: number
    preview_url: string
    track: boolean
    track_number: number
    type: string
    uri: string
}

export interface ISpotifySong {
    added_at: string
    added_by: string
    is_local: boolean
    primary_color: string
    track: ISpotifySongInfos
    video_thumbnail: ISpotifyVideoThumbnail
}

export interface ISpotifyTrack {
    href: string
    items: ISpotifySong[]
    limit: number
    next: string
    offset: number
    previous: string
    total: number
}

export interface ISpotifyPlaylist {
    collaborative: boolean
    description: string
    external_urls: ISpotifyExternalUrl
    href: string
    id: string
    images: ISpotifyImage[]
    name: string
    owner: ISpotifyOwner
    primary_color: string
    public: boolean
    snapshot_id: string
    tracks: ISpotifyTrack
    type: string
    uri: string
}