import React, { useEffect } from 'react'
import { useSession, signIn } from 'next-auth/react'
import spotifyApi, { TOKEN_STATUS } from '../utils/spotify'

const useSpotify = () => {
    const { data: session }: any = useSession()

    useEffect(() => {
        if(session) {
            if(session.error?.error === TOKEN_STATUS.REFRESH_TOKEN_ERROR) {
                signIn()
            }            
            spotifyApi.setAccessToken(session.user.accessToken)
        }
    }, [session])
    return spotifyApi
}

export default useSpotify
