import React from 'react'
import { SpotifyPlaylistContext } from '../contexts/spotify.context'
import { ISpotifyPlaylist } from '../interfaces/spotify.interface';
import Song from './Song';

const Songs = () => {
    const { playlist }: { playlist: ISpotifyPlaylist } = React.useContext(SpotifyPlaylistContext)
    
    return (
        <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
            {playlist?.tracks?.items?.map((track, index) => (
                <Song key={track.track.id} track={track} order={index} />
            ))}
        </div>
    )
}

export default Songs
