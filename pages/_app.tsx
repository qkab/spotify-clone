import { SessionProvider } from 'next-auth/react'
import { useMemo, useState } from 'react'
import { SpotifyCurrentTrackId, SpotifyIsPlaying, SpotifyPlaylistContext, SpotifyPlaylistIdContext } from '../contexts/spotify.context'
import { ISpotifyPlaylist } from '../interfaces/spotify.interface'
import '../styles/globals.css'
function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  const [playlistId, setPlaylistId] = useState("")
  const [playlist, setPlaylist]: [ISpotifyPlaylist, React.Dispatch<any>] = useState(null)
  const [currentTrackId, setCurrentTrackId] = useState(null)
  const [isPlaying, setIsPlaying] = useState(false)

  const providerPlaylistId = useMemo(() => ({ playlistId, setPlaylistId }), [playlistId, setPlaylistId])
  const providerPlaylist = useMemo(() => ({ playlist, setPlaylist }), [playlist, setPlaylist])
  const providerCurrentTrackId = useMemo(() => ({ currentTrackId, setCurrentTrackId }), [currentTrackId, setCurrentTrackId])
  const providerIsPlaying = useMemo(() => ({ isPlaying, setIsPlaying }), [isPlaying, setIsPlaying])

  return (
    <SessionProvider session={pageProps.session}>
      <SpotifyPlaylistIdContext.Provider value={providerPlaylistId}>
        <SpotifyPlaylistContext.Provider value={providerPlaylist}>
          <SpotifyCurrentTrackId.Provider value={providerCurrentTrackId}>
            <SpotifyIsPlaying.Provider value={providerIsPlaying}>
              <Component {...pageProps} />
            </SpotifyIsPlaying.Provider>
          </SpotifyCurrentTrackId.Provider>
        </SpotifyPlaylistContext.Provider>
      </SpotifyPlaylistIdContext.Provider>
    </SessionProvider>
  )
}

export default MyApp
